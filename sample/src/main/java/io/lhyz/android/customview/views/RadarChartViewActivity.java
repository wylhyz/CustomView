package io.lhyz.android.customview.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

import io.lhyz.android.customview.R;
import io.lhyz.android.views.RadarChartView;

public class RadarChartViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_radar);

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setTitle(R.string.title_view_radar);

        RadarChartView view = (RadarChartView) findViewById(R.id.radar_chart);
        Map<String, Integer> data = new TreeMap<>(new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                return lhs.compareTo(rhs);
            }
        });
        data.put("A", 2);
        data.put("B", 4);
        data.put("D", 6);
        data.put("C", 1);
        data.put("E", 3);
        data.put("F", 5);
        data.put("G", 5);
        assert view != null;
        view.initData(data);
    }
}
