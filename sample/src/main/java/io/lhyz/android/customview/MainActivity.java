package io.lhyz.android.customview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import io.lhyz.android.customview.layous.SquareLayoutActivity;
import io.lhyz.android.customview.views.BezierViewActivity;
import io.lhyz.android.customview.views.CircleImageViewActivity;
import io.lhyz.android.customview.views.CompassViewActivity;
import io.lhyz.android.customview.views.RadarChartViewActivity;
import io.lhyz.android.customview.views.SectorChartViewActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button btn = (Button) findViewById(R.id.square_layout);
        assert btn != null;
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNewActivity(SquareLayoutActivity.class);
            }
        });

        btn = (Button) findViewById(R.id.compass_view);
        assert btn != null;
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNewActivity(CompassViewActivity.class);
            }
        });

        btn = (Button) findViewById(R.id.sector_view);
        assert btn != null;
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNewActivity(SectorChartViewActivity.class);
            }
        });

        btn = (Button) findViewById(R.id.bezier_view);
        assert btn != null;
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNewActivity(BezierViewActivity.class);
            }
        });

        btn = (Button) findViewById(R.id.radar_view);
        assert btn != null;
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNewActivity(RadarChartViewActivity.class);
            }
        });

        btn = (Button) findViewById(R.id.circle_image_view);
        assert btn != null;
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNewActivity(CircleImageViewActivity.class);
            }
        });
    }

    public void startNewActivity(Class<?> clazz) {
        Intent intent = new Intent(this, clazz);
        startActivity(intent);
    }
}
