package io.lhyz.android.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;

@SuppressWarnings("unused")
public class SectorChartView extends View {

    private int[] mColors = {
            0xFFCCFF00, 0xFF6495ED,
            0xFFE32636, 0xFF800000,
            0xFF808000, 0xFFFF8C69,
            0xFF808080, 0xFFE6B800,
            0xFF7CFC00
    };

    private float mStartAngle = 0;
    private ArrayList<SectorData> mData;
    private int mWidth, mHeight;
    private Paint mPaint = new Paint();


    public SectorChartView(Context context) {
        super(context);
        init();
    }

    public SectorChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SectorChartView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @SuppressLint("NewApi")
    public SectorChartView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setAntiAlias(true);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec),
                getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (null == mData) {
            return;
        }

        float currentStartAngle = mStartAngle;
        canvas.translate(mWidth / 2, mHeight / 2);
        float r = Math.min(mWidth, mHeight) / 2 * 0.8f;
        RectF rect = new RectF(-r, -r, r, r);

        for (int i = 0; i < mData.size(); ++i) {
            SectorData sector = mData.get(i);
            mPaint.setColor(sector.color);
            canvas.drawArc(rect, currentStartAngle, sector.getAngle(), true, mPaint);
            currentStartAngle += sector.getAngle();
        }
    }

    public void setStartAngle(float startAngle) {
        mStartAngle = startAngle;
        invalidate();
    }

    public void setData(ArrayList<SectorData> data) {
        mData = data;
        initData(mData);
        invalidate();
    }

    private void initData(ArrayList<SectorData> data) {
        if (null == data || data.size() == 0) {
            return;
        }

        float sumValue = 0;
        for (int i = 0; i < data.size(); ++i) {
            SectorData sector = data.get(i);
            sumValue += sector.getValue();

            int j = i % mColors.length;
            sector.setColor(mColors[j]);
        }

        float sumAngle = 0;
        for (int i = 0; i < data.size(); ++i) {
            SectorData sector = data.get(i);

            float percentage = sector.getValue() / sumValue;
            float angle = percentage * 360;

            sector.setPercentage(percentage);
            sector.setAngle(angle);
            sumAngle += angle;
        }
    }


    public static class SectorData {
        private String name;
        private float value;
        private float percentage;

        private int color = 0;
        private float angle = 0.0f;

        public SectorData(String name, float value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public float getValue() {
            return value;
        }

        public void setValue(float value) {
            this.value = value;
        }

        public float getPercentage() {
            return percentage;
        }

        public void setPercentage(float percentage) {
            this.percentage = percentage;
        }

        public int getColor() {
            return color;
        }

        public void setColor(int color) {
            this.color = color;
        }

        public float getAngle() {
            return angle;
        }

        public void setAngle(float angle) {
            this.angle = angle;
        }
    }
}
