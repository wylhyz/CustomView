package io.lhyz.android.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

@SuppressWarnings("unused")
public class CompassView extends View {

    private static final int DEFAULT_BORDER_COLOR = Color.GREEN;
    private static final float DEFAULT_BORDER_WIDTH = 2.0f;
    private static final int DEFAULT_BACKGROUND_COLOR = Color.BLACK;

    int mBorderColor = DEFAULT_BORDER_COLOR;
    float mBorderWidth = DEFAULT_BORDER_WIDTH;

    int mWidth;
    int mHeight;

    Paint mPaint;

    public CompassView(Context context) {
        super(context);
        init(context);
    }

    public CompassView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray array = context.obtainStyledAttributes(
                attrs,
                R.styleable.CompassView,
                0, 0
        );

        try {
            mBorderColor = array.getColor(R.styleable.CompassView_border_color, DEFAULT_BORDER_COLOR);
            mBorderWidth = array.getDimension(R.styleable.CompassView_border_width, DEFAULT_BORDER_WIDTH);
        } finally {
            array.recycle();
        }

        init(context);
    }

    public CompassView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @SuppressLint("NewApi")
    public CompassView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    /**
     * 初始化画笔等相关工具
     **/
    private void init(Context context) {
        mPaint = new Paint();
        mPaint.setColor(mBorderColor);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(10f);//10px
    }

    /**
     * 为属性值设置的get/set方法
     **/
    public int getBorderColor() {
        return mBorderColor;
    }

    public void setBorderColor(int borderColor) {
        mBorderColor = borderColor;
    }

    public float getBorderWidth() {
        return mBorderWidth;
    }

    public void setBorderWidth(float borderWidth) {
        mBorderWidth = borderWidth;
    }


    @Override
    public void layout(int l, int t, int r, int b) {
        super.layout(l, t, r, b);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec),
                getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec));
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
    }

    @Override
    protected void onDraw(Canvas canvas) {

    }

}
