package io.lhyz.android.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

@SuppressWarnings("unused")
public class RadarChartView extends View {
    private static final int DEFAULT_SPIDER_COLOR = 0x88888888;
    private static final int DEFAULT_RADAR_COLOR = 0xFF0000FF;
    private static final int DEFAULT_TEXT_COLOR = DEFAULT_RADAR_COLOR;

    //默认颜色
    int spiderColor = DEFAULT_SPIDER_COLOR;
    int radarColor = DEFAULT_RADAR_COLOR;

    Paint mSpiderWebPaint;
    Paint mTextWebPaint;
    Paint mRadarPaint;
    Paint mRadarLinePaint;

    int maxLevel;
    int centerX, centerY;
    float radius;

    Map<String, Integer> mData;
    ArrayList<String> keys;

    public RadarChartView(Context context) {
        super(context);
        init();
    }

    public RadarChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray array = context.obtainStyledAttributes(
                attrs, R.styleable.RadarChartView,
                0, 0
        );
        try {
            spiderColor = array.getColor(R.styleable.RadarChartView_spiderColor, DEFAULT_SPIDER_COLOR);
            radarColor = array.getColor(R.styleable.RadarChartView_radarColor, DEFAULT_RADAR_COLOR);
        } finally {
            array.recycle();
        }

        init();
    }

    public RadarChartView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @SuppressLint("NewApi")
    public RadarChartView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        //雷达图蜘蛛网络
        mSpiderWebPaint = new Paint();
        mSpiderWebPaint.setColor(spiderColor);
        mSpiderWebPaint.setStyle(Paint.Style.STROKE);
        mSpiderWebPaint.setStrokeWidth(5f);
        mSpiderWebPaint.setAntiAlias(true);

        //雷达图端点文字
        mTextWebPaint = new Paint();
        mTextWebPaint.setColor(radarColor);
        mTextWebPaint.setStyle(Paint.Style.FILL);
        mTextWebPaint.setStrokeWidth(5f);
        mTextWebPaint.setTextSize(50);
        mTextWebPaint.setAntiAlias(true);

        //雷达图数据内容填充色
        mRadarPaint = new Paint();
        mRadarPaint.setColor(radarColor);
        mRadarPaint.setAlpha(128);// 将填充色设置为半透明（alpha值是从0-255）
        mRadarPaint.setStyle(Paint.Style.FILL);
        mRadarPaint.setStrokeWidth(10f);
        mRadarPaint.setAntiAlias(true);

        //雷达图数据边线
        mRadarLinePaint = new Paint();
        mRadarLinePaint.setColor(radarColor);
        mRadarLinePaint.setStyle(Paint.Style.STROKE);
        mRadarLinePaint.setStrokeWidth(5f);
        mRadarLinePaint.setAntiAlias(true);
    }

    public void initData(Map<String, Integer> data) {
        maxLevel = Collections.max(data.values()) + 1;
        mData = data;
        keys = new ArrayList<>(mData.keySet());
        invalidate();
        requestLayout();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        centerX = w / 2;
        centerY = h / 2;
        radius = Math.min(w, h) / 2 * 0.8f;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec),
                getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (null == mData) {
            throw new IllegalArgumentException("initData() function has not be called");
        }

        drawSpiderWeb(canvas);

        drawRadar(canvas);
    }

    private void drawRadar(Canvas canvas) {
        float degree = (float) (2 * Math.PI / mData.size()); //必须是弧度值
        float x;
        float y;
        float deg;
        Path path = new Path();
        Path line = new Path();

        float r = radius / maxLevel;
        int i = 0;
        for (int v : mData.values()) {
            deg = degree * i;
            x = (float) (centerX + v * r * Math.sin(deg));
            y = (float) (centerY + v * r * Math.cos(deg));
            canvas.drawCircle(x, y, 10f, mRadarPaint);
            if (0 == i++) {
                path.moveTo(x, y);
            } else {
                path.lineTo(x, y);
            }
        }
        path.close();
        canvas.drawPath(path, mRadarPaint);
        canvas.drawPath(path, mRadarLinePaint);
    }

    /**
     * 绘制蜘蛛网络
     */
    private void drawSpiderWeb(Canvas canvas) {
        float degree = (float) (2 * Math.PI / mData.size()); //必须是弧度值

        float x;
        float y;
        float deg;
        Path path = new Path();

        float r = radius / maxLevel;
        float curR;
        for (int i = 1; i <= maxLevel; ++i) {
            path.reset();
            curR = r * i;
            for (int j = 0; j < mData.size(); ++j) {
                deg = degree * j;
                x = (float) (centerX + curR * Math.sin(deg));
                y = (float) (centerY + curR * Math.cos(deg));
                if (j == 0) {
                    path.moveTo(x, y);
                } else {
                    path.lineTo(x, y);
                }
            }
            path.close();
            canvas.drawPath(path, mSpiderWebPaint);
        }

        for (int i = 0; i < mData.size(); ++i) {
            deg = degree * i;
            x = (float) (centerX + radius * Math.sin(deg));
            y = (float) (centerY + radius * Math.cos(deg));
            canvas.drawLine(centerX, centerY, x, y, mSpiderWebPaint);

            canvas.drawText(keys.get(i), x, y, mTextWebPaint);
        }
    }

    /**
     * 这是硬编码设置一个颜色为其半透明的方法
     *
     * @param color 不透明的颜色
     * @return 半透明的颜色
     */
    @Deprecated
    private static int colorTranslucent(int color) { //将一个颜色值改为他的半透明颜色（元颜色不是半透明）,假设半透明的最高位为88（10001000）
        color &= 0x88FFFFFF; //将最高位置为x000x000
        color |= 0x88000000; //将最高位置为1xxx1xxx
        return color;
    }
}
