package io.lhyz.android.layouts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * 方形适应布局，布局的高度总是和宽度相等，因此只要是宽度被确定，那么高度一定和宽度相等，
 * 试用于GridView和其他需要子View
 * 适应宽度但是宽度不固定的情况，这个布局可以保证子View的宽高相等
 */
@SuppressWarnings("unused")
public class SquareLayout extends RelativeLayout {

    public SquareLayout(Context context) {
        super(context);
    }

    public SquareLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @SuppressLint("NewApi")
    public SquareLayout(Context context, AttributeSet attrs,
                        int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        /**此方法必须由onMeasure调用，用来存储实际子View测量得到的宽高值**/
        setMeasuredDimension(
                getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec),
                getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec));

        int childWidthSize = getMeasuredWidth();

        heightMeasureSpec = widthMeasureSpec =
                MeasureSpec.makeMeasureSpec(childWidthSize, MeasureSpec.EXACTLY);

        /**必须覆盖父类的方法以防止我们覆盖原layout默认的行为**/
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
    }
}
