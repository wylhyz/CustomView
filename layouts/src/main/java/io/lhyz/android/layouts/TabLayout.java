package io.lhyz.android.layouts;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class TabLayout extends RelativeLayout {

    private ViewPager mViewPager;

    private TabStrip mTabStrip;

    public TabLayout(Context context) {
        super(context);
    }

    public TabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mTabStrip = new TabStrip(context);
        mTabStrip.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_light));
        addView(mTabStrip, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    public TabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public TabLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setViewPager(ViewPager viewPager) {
        mViewPager = viewPager;
        if (viewPager != null) {
            mViewPager.setOnPageChangeListener(new InternalViewPagerListener());
        }
    }

    private TextView createDefaultTextView(Context context) {
        TextView textView = new TextView(context);
        textView.setGravity(Gravity.CENTER);
        textView.setBackgroundColor(Color.BLUE);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setTextColor(getResources().getColor(android.R.color.darker_gray));
        textView.setAllCaps(true);
        return textView;
    }

    private void createDefaultTextTabView() {
        final PagerAdapter adapter = mViewPager.getAdapter();
        for (int i = 0; i < adapter.getCount(); ++i) {
            TextView titleView = createDefaultTextView(getContext());
            titleView.setText(adapter.getPageTitle(i));
            if (i == 0) {
                titleView.setTextColor(getResources().getColor(android.R.color.white));
            }
            titleView.setOnClickListener(new TabClickListener());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
            mTabStrip.addView(titleView, params);
        }
    }


    private class InternalViewPagerListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            int tabStripChildCount = mTabStrip.getChildCount();
            if ((tabStripChildCount == 0) || (position < 0) || (position >= tabStripChildCount)) {
                return;
            }
            mTabStrip.onViewPagerChanged(position, positionOffset);
        }

        @Override
        public void onPageSelected(int position) {
            for (int i = 0; i < mTabStrip.getChildCount(); ++i) {
                if (i == position) {
                    ((TextView) mTabStrip.getChildAt(position)).setTextColor(getResources().getColor(android.R.color.white));
                } else {
                    ((TextView) mTabStrip.getChildAt(i)).setTextColor(getResources().getColor(android.R.color.darker_gray));
                }
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    }


    private class TabClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            for (int i = 0; i < mTabStrip.getChildCount(); i++) {
                if (v == mTabStrip.getChildAt(i)) {
                    mViewPager.setCurrentItem(i);
                    return;
                }
            }
        }
    }


    public static class TabStrip extends LinearLayout {
        private float mOffset;
        private int mPosition;
        private Paint mBorderPaint;

        public TabStrip(Context context) {
            super(context);
            setWillNotDraw(false);
        }

        public TabStrip(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public TabStrip(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            mBorderPaint = new Paint();
            mBorderPaint.setAntiAlias(true);
            mBorderPaint.setStyle(Paint.Style.FILL);
            mBorderPaint.setColor(getResources().getColor(android.R.color.white));
        }

        @Override
        protected void onDraw(Canvas canvas) {
            final int height = getHeight();
            final int childCount = getChildCount();
            if (childCount > 0) {
                View selectedTitle = getChildAt(mPosition);
                int left = selectedTitle.getLeft();
                int right = selectedTitle.getRight();

                if (mOffset > 0f && mPosition < (getChildCount() - 1)) {

                    View nextTitle = getChildAt(mPosition + 1);
                    left = (int) (mOffset * nextTitle.getLeft() +
                            (1.0f - mOffset) * left);
                    right = (int) (mOffset * nextTitle.getRight() +
                            (1.0f - mOffset) * right);
                }

                canvas.drawRect(left, height - 2 * getResources().getDisplayMetrics().density, right, height, mBorderPaint);
            }
        }

        public void onViewPagerChanged(int position, float positionOffset) {
            mPosition = position;
            mOffset = positionOffset;
            invalidate();
        }
    }
}
