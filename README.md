# CustomView 集合

个人学习View组合，自定义View，自定义ViewGroup和改造Layout的项目集合，所有view相关的都在views模块下，所有layout相关的都在layous模块下。


(为了测试如何部署Travis的持续集成操作，已经加入相关文件，具体请看 .travis.yml 文件如何使用)

Contents
===========
## 1.CompassView （指南针）(暂无)
![](/arts/)

## 2.SectorChartView （扇形图）
![](/arts/sector.png)

## 3.BezierView (贝塞尔简介)
![](/arts/bezier.png)

## 4.RadarChartView (雷达图)
![](/arts/radar.png)

## 5.SquareLayout (自适应等宽高)
![](/arts/square.png)